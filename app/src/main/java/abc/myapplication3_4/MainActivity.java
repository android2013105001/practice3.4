package abc.myapplication3_4;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView) findViewById(R.id.listView);

        ArrayList<String> arrName = new ArrayList<String>();
        arrName.add("강슬기");
        arrName.add("김정은");
        arrName.add("김아무개");
        arrName.add("이아무개");
        arrName.add("박아무개");
        arrName.add("최아무개");
        arrName.add("정아무개");
        arrName.add("탁아무개");
        arrName.add("권아무개");

        ArrayAdapter<String> adapName = new ArrayAdapter<String>(
                this,
                R.layout.item,
                R.id.textView,
                arrName //포함된 리스트
        );

        listView.setAdapter(adapName);
    }
}
